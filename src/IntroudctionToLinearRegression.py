import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.formula.api as sfa

df = pd.read_csv("http://www-bcf.usc.edu/~gareth/ISL/Advertising.csv", index_col=0)
print(df)

# Plotting in single window

df.plot(kind="scatter", x="TV", y="Sales")
df.plot(kind="scatter", x="Radio", y="Sales")
df.plot(kind="scatter", x="Newspaper", y="Sales")

# Plot in the same window
fig, axs = plt.subplots(1, 3, sharey=True)
df.plot(kind="scatter", x="TV", y="Sales", ax = axs[0], figsize = (16,8))
df.plot(kind="scatter", x="Radio", y="Sales", ax = axs[1])
df.plot(kind="scatter", x="Newspaper", y="Sales", ax =axs[2])


############  STATSMODELS LIBRARY ###########################################
# Build a linear regression model to predict Sales based on investment on TV data
lm = sfa.ols(formula="Sales~TV", data=df).fit()
print(lm.params)

# create a DataFrame with the minimum and maximum values of TV
X_new = pd.DataFrame({'TV': [df.TV.min(), df.TV.max()]})
preds = lm.predict(X_new)

# first, plot the observed data
df.plot(kind='scatter', x='TV', y='Sales')
# then, plot the least squares line
plt.plot(X_new, preds, c='red', linewidth=2)

# Try to make a prediction with new data
X_new = pd.DataFrame({'TV': [30, 50, 70]})
print(lm.predict(X_new))

# Build a linear regression model to predict Sales based on investment on TV, Radio, and Newspaper data
lm2 = sfa.ols(formula="Sales~TV+Radio+Newspaper", data=df).fit()
print(lm2.params)

# Try to make a prediction with new data
X_new = pd.DataFrame({'TV': [30, 50, 70], 'Radio':[20, 30, 40], 'Newspaper':[15, 30, 25]})
#print(lm2.predict(X_new))

############  SCIKITLEARN LIBRARY ###########################################
from sklearn.linear_model import LinearRegression

x_column_name = ["TV", "Radio", "Newspaper"]
x = df[x_column_name]
y = df["Sales"]

lm = LinearRegression()
lm.fit(x, y)

print(lm.intercept_)
print(lm.coef_)