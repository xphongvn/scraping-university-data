import json
data = '''{
  "name" : "Chuck",
  "phone" : {
    "type" : "intl",
    "number" : "+1 734 303 4456"
   },
   "email" : {
     "hide" : "yes"
} }'''   # data is in string format

info = json.loads(data) # convert the string into json format
print 'Name:',info["name"]   # access the name key
print 'Email:',info["email"]
print 'Hide:',info["email"]["hide"]

# when we download data from the internet, they are in string format, like the example below
input = '''[
{   "id" : "001",
    "x" : "2",
    "name" : "Chuck"
},
{  "id" : "009",
   "x" : "7",
   "name" : "Noemie"
}
]'''
info = json.loads(input) # convert the string data into json format to access it more easily
print 'User count:', len(info)  # print the length of the data
for item in info:               # print the different parts within the data
    print 'Name', item['name']
    print 'Id', item['id']
    print 'Attribute', item['x']

