import urllib
import json

serviceurl = 'http://maps.googleapis.com/maps/api/geocode/json?'

while True:
    address = raw_input('Enter Location: ')     # enter location with your keyboard
    if len(address) < 1 : break                 # if length is smaller than 1, it is not valid so it is going to break (leaves the loop)

    url = serviceurl + urllib.urlencode({'sensor':'false','address':address})     # add the parameters to the url above
    print(url)                      # print url in order to know what the url looks like

    uh = urllib.urlopen(url)        # opens the url and retrieve the data
    data = uh.read()                # read the data
    print 'Retrieved', len(data), 'characters'  # print how many characters in the data

    try:                            # use 'try' so that if there is an error, it runs another way
        js = json.loads(str(data))  # try to load the data as a json format
    except:                         # after 'except' we will handle the error
        js = None                   # if we cannot load the data as a json format, we will give it a 'None' value

    if 'status' not in js or js['status'] != 'OK' :     # check whether the data has the status OK
        print '==== Failure to Retrieve ===='           # if it does not, the request is not valid
        print data
        continue

    print json.dumps(js, indent=4)      # print the data beautifully with indentation

    lat = js["results"][0]["geometry"]["location"]["lat"]   # get the latitude
    lng = js["results"][0]["geometry"]["location"]["lng"]   # get the longitude
    print 'lat', lat, 'lng', lng        # print the latitude and longitude
    location = js["results"][0]['formatted_address']   # get the location
    print location          # print the location