from bs4 import BeautifulSoup
import os

file_names = os.listdir('./html_folder')

uni_urls = []
for file_name in file_names:
    print file_name
    html = open('./html_folder/' + file_name).read()
    # organize the html with BeautifulSoup library
    soup = BeautifulSoup(html, 'html.parser')
    try:
        contents = soup.find('div', id='main-content').find_all('li', class_='item result school')
        for c in contents:
            content = c.find('a', href=True)
            uni_url = content["href"]
            print(uni_url)
            uni_urls.append(uni_url)
        if len(contents) == 0:
            contents = soup.find('div', id='main-content').find_all('li', class_='item result school ')
            for c in contents:
                content = c.find('a', href=True)
                uni_url = content["href"]
                print(uni_url)
                uni_urls.append(uni_url)
        if len(contents) == 0:
            uni_url = "Cannot find url of " + file_name
            print(uni_url)
            uni_urls.append(uni_url)
    except:
        print("Something is WRONG")

file_name = '/Users/noemievoss/Python Tutorial/src/university_urls_all.csv'
file = open(file_name, mode='w')
for url in uni_urls:
    file.write(url + "\n")
file.close()