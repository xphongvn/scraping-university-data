import re

hand = open("src/essay.txt")
# Use re.search() to look for a search string in a big string
for line in hand:
    line = line.rstrip()                       # remove all the special characters of return \n
    if re.search('Inwagen', line):             # re.search returns an object if the big string contains the searched string
        print(line)

hand = open("src/essay.txt")
# use '^' to specify the searched string to start from the beginning of the big string
for line in hand:
    line = line.rstrip()                       # remove all the special characters of return \n
    if re.search('^If', line):                 # re.search returns an object if the big string contains the searched string
        print(line)


x = 'My 2 favorite numbers are 35 and 83'
y = re.findall('[0-9]+', x)                    # return a python list of the matched pattern
print(y)

hand = open("src/essay.txt")
for line in hand:
    line = line.rstrip()
    y = re.findall('\S+[0-9]+', line)
    print(y)

x = 'From: Using the : character'
y = re.findall('^F.+:', x)                     # * and + give you the longest possible string which matches the pattern
print(y)
y = re.findall('^F.+?:', x)                    # * and + give you the shortest possible string which matches the pattern
print(y)

x = 'From : stephen.marquard@uct.ac.za and noemievoss@gmail.com Sat Jan  5 09:14:16 2008'
y = re.findall('\S+@\S+', x)                   # the pattern that gives you email addresses
print(y)
y = re.findall('^From :.*?(\S+@\S+)', x)
# find the pattern and then extract only the part of the string that is between the parenthesise
print(y)

# one pattern can be written in many ways. eg: \S is similar to [^ ]
y = re.findall('\S+@(\S+)', x)
y = re.findall('@(\S*)', x)
y = re.findall('@([^ ]*)', x)                  # if the @ is in the [] it means 'not those following characters'
print(y)

y = re.findall('^From :.*?\S+@(\S+)', x)
print(y)

x = 'We just received $10.00 for cookies'
y = re.findall('\$[0-9.]+', x)                 # add a \ to make a special character behave normally. eg: '\$', '\*'
print(y)