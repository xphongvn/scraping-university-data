import urllib2
import os
import re
from bs4 import BeautifulSoup
import pandas as pd
import sys

def extract_uni_info(url, file):
    try:
        # open the webpage that contain the Princeton University information
        html = urllib2.urlopen(url)
        # organize the html with BeautifulSoup library
        soup = BeautifulSoup(html, 'html.parser')

        # find the div tag with class = "school_name_holder" and jump into span tag.
        uni_name = soup.find('div', class_="school_name_holder").find('span').contents[0].string.strip()
        #print(uni_name)
        uni_name = re.sub(',','',uni_name)
        # find the div tag with id...
        location = soup.find('div', id='factsLocation').find('div', class_='fact').contents[0].string.strip()
        location = re.sub(',','',location)
        #print(location)
        type = soup.find('div', id='factsType').find('div', class_='fact').contents[0].string.strip()
        #print(type)
        setting = soup.find('div', id='factsSetting').find('div', class_='fact').contents[0].string.strip()
        #print(setting)

        # find the div withe the id ...
        # however, in the div with class = 'fact', there are 2 identical span tag, so we need to use find_all
        female_and_male_ratio = soup.find('div', id='factsRatio').find('div', class_='fact').find_all('span')
        if not female_and_male_ratio[0].contents[0].string.strip() == "Not Reported":
            #print(female_and_male_ratio)
            # take the data from the first span tag
            female_ratio = female_and_male_ratio[0].contents[0].string.strip()
            #print(female_ratio)
            # take the data from the second span tag
            male_ratio = female_and_male_ratio[1].contents[0].string.strip()
            #print(male_ratio)
        else:
            female_ratio = ''
            male_ratio = ''

        total_students = soup.find('div', id='factsEnrollment').find('div', class_='fact').contents[0].string.strip()
        #print(total_students)
        total_students = re.sub(',', '', total_students)

        tuition = soup.find('div', id='factsTuition').find('div', class_='fact')
        if tuition:
            in_state_tuition = soup.find('div', id='factsTuition').find('div', class_='fact').contents[5].contents[2].string.strip()
            #print(in_state_tuition)
            in_state_tuition = re.sub(',','',in_state_tuition)

            out_of_state_tuition = soup.find('div', id='factsTuition').find('div', class_='fact').contents[5].contents[4].string.strip()
            #print(out_of_state_tuition)
            out_of_state_tuition = re.sub(',', '', out_of_state_tuition)
        else:
            in_state_tuition = ''
            out_of_state_tuition = ''

        fall_application_deadline = soup.find('div', id='factsDeadline').find('div', class_='fact').contents[0].string.strip()
        #print(fall_application_deadline)

        acceptance_rate = soup.find('div', id='factsAccept').find('div', class_='fact').contents[0].string.strip()
        #print(acceptance_rate)

        sat_math = soup.find('div', id='factsSATmath').find('div', class_='fact').contents[0].string.strip()
        #print(sat_math)

        sat_reading = soup.find('div', id='factsSATreading').find('div', class_='fact').contents[0].string.strip()
        #print(sat_reading)

        sat_writing = soup.find('div', id='factsSATwriting').find('div', class_='fact').contents[0].string.strip()
        #print(sat_writing)

        act_composite = soup.find('div', id='factsACT').find('div', class_='fact').contents[0].string.strip()
        #print(act_composite)

        write_to_file_info(uni_name, location, type, setting, female_ratio, male_ratio, total_students, in_state_tuition,
                       out_of_state_tuition, fall_application_deadline, acceptance_rate, sat_math, sat_reading,
                       sat_writing, act_composite, file)

    except:
        print "Unexpected error:", sys.exc_info()[0]
        print "Something is wrong with this URL: " + url

def write_to_file_info(uni_name,location,type,setting,female_ratio,male_ratio,total_students,in_state_tuition,
                       out_of_state_tuition,fall_application_deadline,acceptance_rate,sat_math,sat_reading,
                       sat_writing,act_composite, file):
    if not uni_name: uni_name = 'NA'
    if not location: location = 'NA'
    if not type: type = 'NA'
    if not setting: setting = 'NA'
    if not female_ratio: female_ratio = 'NA'
    if not male_ratio: male_ratio = 'NA'
    if not total_students: total_students = 'NA'
    if not in_state_tuition: in_state_tuition = 'NA'
    if not out_of_state_tuition: out_of_state_tuition = 'NA'
    if not fall_application_deadline: fall_application_deadline = 'NA'
    if not acceptance_rate: acceptance_rate = 'NA'
    if not sat_math: sat_math = 'NA'
    if not sat_reading: sat_reading = 'NA'
    if not sat_writing: sat_writing = 'NA'
    if not act_composite: act_composite = 'NA'

    file.write(uni_name + ',' + location + ',' + type + ',' + setting + ',' + female_ratio + ',' + male_ratio + ',' +
               total_students + ',' + in_state_tuition + ',' + out_of_state_tuition + ',' + fall_application_deadline
               + ',' + acceptance_rate + ',' + sat_math + ',' + sat_reading + ',' + sat_writing + ',' + act_composite +
                '\n')


# main program starts from here

# in this program, we list the urls of the different universities
# list_urls = ['https://www.petersons.com/college-search/princeton-university-000_10000565.aspx',
#              'https://www.petersons.com/college-search/harvard-university-000_10000215.aspx',
#              'https://www.petersons.com/college-search/stanford-university-000_10000271.aspx',
#              'https://www.petersons.com/college-search/yale-university-000_10000250.aspx',
#              'https://www.petersons.com/college-search/massachusetts-institute-of-technology-000_10004085.aspx',
#              'https://www.petersons.com/college-search/university-of-california-berkeley-000_10004086.aspx',
#              'https://www.petersons.com/college-search/bloomsburg-university-of-pennsylvania-000_10002075.aspx',
#              'https://www.petersons.com/college-search/columbia-university-000_10000001.aspx',
#              'https://www.petersons.com/college-search/new-york-university-000_10000134.aspx',
#              'https://www.petersons.com/college-search/carnegie-mellon-university-000_10000483.aspx']
df_url = pd.read_csv("/Users/noemievoss/Python Tutorial/src/university_urls_all.csv", header=None)
df_url[0] = df_url[0].astype('str')
# get rid of rows that contain 'online-schools'
df_url = df_url[~df_url[0].str.contains('online-schools')]
# get rid of rows that contain 'Cannot find'
df_url = df_url[~df_url[0].str.contains('Cannot find')]
# add the domain before the url
df_url[0] = 'https://www.petersons.com' + df_url[0]
# turn series into a list
list_urls = df_url[0].tolist()

# check whether the file is existing, and if it is, delete it
if os.path.exists('university_information.csv'):
    os.remove('university_information.csv')

# we open a file and write the headers to the file
file = open('university_information.csv', mode = 'w')
file.write('University Name,Location,Type,Setting,Female Ratio,Male Ratio,Total Students,In State Tuition,'
           'Out of State Tuition,Fall Application Deadline,Acceptance Rate,Sat Math,Sat Reading,Sat Writing,'
           'Act Composite\n')

# for each of the urls in the url list, we will extract the information and write them into the file
for url in list_urls:
    extract_uni_info(url,file)
    print url

# close and save the file
file.close()