import urllib2
import os
import re
from bs4 import BeautifulSoup
import pandas as pd
import sys

def extract_uni_info(url, file):
    try:
        # open the webpage that contain the Princeton University information
        html = urllib2.urlopen(url)
        # organize the html with BeautifulSoup library
        soup = BeautifulSoup(html, 'html.parser')

        # find the div tag with class = "school_name_holder" and jump into span tag.
        uni_name = soup.find('h1', class_="mb-1 pl-0 h3 text-white").contents[0].string.strip()
        #print(uni_name)
        uni_name = re.sub(',','',uni_name)
        # find the div tag with id...
        location = soup.find('div', class_='address').contents[4]
        location = re.sub(',','',location)
        #print(location)
        setting = soup.find('div', class_='header-facts--value d-block mt-2 mb-0 h3').contents[0].string.strip()
        #print(setting)

        total_students = soup.find('div', class_='header-facts--value mb-0 h3').contents[0].string.strip()
        #print(total_students)
        total_students = re.sub(',', '', total_students)

        admissions_all = soup.find('section', id="admissions")
        if admissions_all:
            admissions = admissions_all.find_all('div', class_="profile--main--fact-value h2")
            applied_students = admissions[0].contents[0].string.strip()
            applied_students = re.sub(',', '', applied_students)
            accepted_students = admissions[1].contents[0].string.strip()
            accepted_students = re.sub(',', '', accepted_students)
            enrolled_students = admissions[2].contents[0].string.strip()
            enrolled_students = re.sub(',', '', enrolled_students)
            gpa = admissions[len(admissions)-1].contents[0].string.strip()

            acceptance_rate = str(round(float(accepted_students) / float(applied_students) * 100,4))
        else:
            applied_students = ""
            accepted_students = ""
            enrolled_students = ""
            acceptance_rate = ""
            gpa = ""

        write_to_file_info(uni_name, location, setting, total_students, applied_students, accepted_students,
                           enrolled_students, acceptance_rate, gpa)

    except Exception as e:
        print("Unexpected error:", sys.exc_info()[0],str(e))
        print("Something is wrong with this URL: " + url)

def write_to_file_info(uni_name, location, setting, total_students, applied_students, accepted_students,
                           enrolled_students, acceptance_rate, gpa):
    if not uni_name: uni_name = 'NA'
    if not location: location = 'NA'
    if not setting: setting = 'NA'
    if not total_students: total_students = 'NA'
    if not applied_students: applied_students = 'NA'
    if not accepted_students: accepted_students = 'NA'
    if not enrolled_students: enrolled_students = 'NA'
    if not acceptance_rate: acceptance_rate = 'NA'
    if not gpa: gpa = 'NA'

    str_to_write1 = uni_name + ',' + location + ',' + setting + ',' + total_students + ',' + applied_students + ',' + accepted_students + ',' + enrolled_students + ',' + acceptance_rate + ',' + gpa + '\n'
    str_to_write = str_to_write1.encode('ascii','ignore')
    file.write(str_to_write)
    print(str_to_write)


# main program starts from here

# in this program, we list the urls of the different universities
# list_urls = ['https://www.petersons.com/college-search/princeton-university-000_10000565.aspx',
#              'https://www.petersons.com/college-search/harvard-university-000_10000215.aspx',
#              'https://www.petersons.com/college-search/stanford-university-000_10000271.aspx',
#              'https://www.petersons.com/college-search/yale-university-000_10000250.aspx',
#              'https://www.petersons.com/college-search/massachusetts-institute-of-technology-000_10004085.aspx',
#              'https://www.petersons.com/college-search/university-of-california-berkeley-000_10004086.aspx',
#              'https://www.petersons.com/college-search/bloomsburg-university-of-pennsylvania-000_10002075.aspx',
#              'https://www.petersons.com/college-search/columbia-university-000_10000001.aspx',
#              'https://www.petersons.com/college-searact_composite = soup.find('div', id='factsACT').find('div', class_='fact').contents[0].string.strip()ch/new-york-university-000_10000134.aspx',
#              'https://www.petersons.com/college-search/carnegie-mellon-university-000_10000483.aspx']
df_url = pd.read_csv("university_urls_all.csv", header=None)
df_url[0] = df_url[0].astype('str')
# get rid of rows that contain 'online-schools'
df_url = df_url[~df_url[0].str.contains('online-schools')]
# get rid of rows that contain 'Cannot find'
df_url = df_url[~df_url[0].str.contains('Cannot find')]
# add the domain before the url
df_url[0] = 'https://www.petersons.com' + df_url[0]
# turn series into a list
list_urls = df_url[0].tolist()

# check whether the file is existing, and if it is, delete it
if os.path.exists('university_information_new.csv'):
    os.remove('university_information_new.csv')

# we open a file and write the headers to the file
with open('university_information_new.csv', mode = 'w') as file:
    file.write('uni_name,location,setting,total_students,applied_students,accepted_students,'
               'enrolled_students,acceptance_rate,gpa\n')

    # for each of the urls in the url list, we will extract the information and write them into the file
    for url in list_urls:
        extract_uni_info(url,file)
        print url
