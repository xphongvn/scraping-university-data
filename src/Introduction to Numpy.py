import numpy as np

# A simple 1D array
data1 = [6, 7.5, 8, 0, 1]           # list
array1 = np.array(data1)            # array

# A 2D array
data2 = [range(1, 5), range(5, 9)]  # list of lists
array2 = np.array(data2)            # 2D array
list2 = array2.tolist()             # convert array back into list

# Examine the array
print(array1.dtype)                 # check the type of element in the array
print(array2.dtype)                 # check the type of element in the array
print(array2.ndim)                  # check the dimension of the array
print(array2.shape)                 # check how many rows and how many columns of an array
print(array2.size)                  # check how many elements in the array
print(len(array2))                  # check how many rows in the array

# create special arrays
a1 = np.zeros(10)                   # make an 1d array of 10 zeros
a2 = np.zeros((7, 5))               # make a 2d array with 7 rows and 5 columns of zeros
a3 = np.ones(10)                    # make an 1d array of 10 ones
a4 = np.arange(5)                   # make a 1d array with values from 0 to 4
a5 = np.arange(3,10)                # make a 1d array with values from 3 to 9
a6 = np.arange(3,10,2)              # make a 1d array with values from 3 to 9, going up by 2 for each value

print(a4[2])                        # print the 3RD element in the list (because index in python starts from 0)
print(array2[1])                    # print the whole second row in a 2d array
print(array2[1,2])                  # print the 3RD element in the second row of a 2d array
print(a5[2:6])                      # print from the 3rd element to the 7th element

a = a5[2:8]                         # create variable 'a' which is a reference to elements 3 to 9 of a5
a[:] = 10     # when we change the value of 'a' to 10, it also affects a5 because they are referenced to the same memory
a5 = np.arange(3,10)                # make a 1d array with values from 3 to 9
a = a5[2:8].copy()                  # create a copy (variable 'a') which does not reference to the same memory as a5
a[:] = 10     # when we change the value of 'a' to 10, it doesn't affect a5 because it is referenced to different memory


# array mathematics -- mathematical expressions in arrays are applied element by element
a = np.array([1.,2,3])
b = np.array([5.,2,6])
print(a+b)
print(a-b)
print(a*b)
print(a/b)
print(a%b)                          # find the remainder of the division
print(b**a)                         # b to the power of a

a = np.array([[1.,2], [3,4]])
b = np.array([[2.,0], [1,3]])
print(a*b)

a = np.array([[1., 2], [3, 4], [5, 6]])
b = np.array([-1., 3])
print(a+b)

a = np.array([1, 4, 9.])
print(np.sqrt(a))

a = np.array([1, -4, 9.])
print(np.sign(a))                   # show if the values are positive or negative

a = np.array([1.1, 1.5, 1.9])
print(np.floor(a))                  # round down to whole number
print(np.ceil(a))                   # round up to whole number
print(np.rint(a))                   # round to nearest whole number

print(np.pi)
print(np.e)

# statistic
a = np.array([4,2,5,1,3.])
print(max(a))
print(a.max())
print(a.argmax())
print(a.mean())
print(a.min())
print(min(a))
print(a.argmin())
print(a.std())
print(a.sum())

b = np.array([[2,7,6],
              [5,9,1],
              [2,6,4.]])
print(b.max())                      # if nothing is specified, it will look for the max value of the whole array
print(b.max(axis=0))                # axis=0 means column
print(b.max(axis=1))                # axis=1 means row
print(b.min())                      # if nothing is specified, it will look for the min value of the whole array
print(b.min(axis=0))                # axis=0 means column
print(b.min(axis=1))                # axis=1 means row
print(b.mean())                     # if nothing is specified, it will look for the mean value of the whole array
print(b.mean(axis=0))               # axis=0 means column
print(b.mean(axis=1))               # axis=1 means row
print(b.sum())
print(b.sum(axis=0))               # axis=0 means column
print(b.sum(axis=1))               # axis=1 means row

# randomization
print(np.random.rand())            # get a random value from 0 to 1
print(np.random.rand(5))           # get 5 random values from 0 to 1
rnd = np.random.rand(1000)         # get 1000 random values
sum(rnd <= 0.5)                    # count the number of values less than or equal to 0.5 (because True means 1, False means 0)
sum(rnd > 0.5)                     # count the number of values more than 0.5

