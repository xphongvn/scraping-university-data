import urllib2
import os
import re
from bs4 import BeautifulSoup

# open the webpage that contain the Princeton University information
html = urllib2.urlopen('http://doors.stanford.edu/~sr/universities.html')
# organize the html with BeautifulSoup library
soup = BeautifulSoup(html, 'html.parser')

list_uni = soup.find('ol').find_all('li')
uni_names = []
for uni in list_uni:
    uni_names.append(uni.contents[0].contents[0].string.strip())

print(len(uni_names))

# check whether the file is existing, and if it is, delete it
if os.path.exists('university_names.csv'):
    os.remove('university_names.csv')

# we open a file and write the headers to the file
file = open('university_names.csv', mode = 'w')
file.write('University Name\n')

for uni_name in uni_names:
    file.write(uni_name + '\n')