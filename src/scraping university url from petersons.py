import pandas as pd
from bs4 import BeautifulSoup
from selenium import webdriver
import time

# read the university names from csv file
uni_names = pd.read_csv("/Users/noemievoss/Python Tutorial/src/university_names.csv")
n_uni = len(uni_names)
base_url = "https://www.petersons.com/search/schools?searchtype=12&searchterm="

# Create a driver for Firefox
driver = webdriver.Firefox(executable_path = '/Users/noemievoss/Python Tutorial/src/geckodriver')
driver.implicitly_wait(10)

uni_urls = []

# Make a loop for all university names
for i in xrange(n_uni):
    # get one of the university names from the list
    uni_name = uni_names["University Name"][i]
    # attach the university name after the base url
    sub_name = uni_name.replace(" ", "%20")
    url = base_url + sub_name
    # Use Firefox to open the url
    driver.get(url)
    time.sleep(10)
    elem = driver.find_element_by_xpath("//*")
    html = elem.get_attribute("outerHTML")
    html = html.encode('utf-8')
    file_name = '/Users/noemievoss/Python Tutorial/src/html_folder/' + uni_name + '.html'
    file = open(file_name, mode='w')
    file.write(html)
    file.close()
    if html:
       print("Written HTML of " + uni_name)
    else:
       print("Nothing found.")


    # organize the html with BeautifulSoup library
    soup = BeautifulSoup(html, 'html.parser')
    try:
        content = soup.find('div', id='main-content').find('li', class_='item result school').find('a', href = True)
        uni_url = content["href"]
    except:
        uni_url = "Cannot find url of " + uni_name
    print(uni_url)
    uni_urls.append(uni_url)

file_name = '/Users/noemievoss/Python Tutorial/src/university_urls.csv'
file = open(file_name, mode='w')
for url in uni_urls:
    file.write(url + "\n")
file.close()