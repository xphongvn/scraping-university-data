import pandas as pd
from matplotlib import pyplot as plt
import numpy as np

df = pd.read_csv('/Users/noemievoss/Python Tutorial/src/university_information.csv')
df.drop_duplicates(inplace = True)
df.describe()
df.head()
df.shape
########################################################################################################################
# Report on missing data
df.isnull().sum() # Check missing data

# Plot the histogram of total students.
# Most universities have between 0 and 3000 students. Very few universities have over 30000 students, the highest being
# around 70000.
plt.figure()
plt.hist(df["Total Students"].dropna(), bins = 50)
plt.title('Histogram of Total Students in American Universities')
plt.xlabel("Number of Students")
plt.ylabel("Number of Universities")
plt.show()


# Analyze the ACT Composite
df["Act Composite"].describe() # there are some outliers, we should remove those greater than 36
df[df["Act Composite"] > 36] # there is one school which swapped the SAt Reading and ACT Composite scores
df.set_value(488,"Act Composite", 22)
df.set_value(488,"Sat reading", 488)

# Plot the histogram of ACT composite
# The most common average ACT Composite score is around 22. Very few universities have an average ACT Composite score
# below 20, and very few require the full 36 score. One university has a score of around 0, though this is most likely
# an outlier.
plt.figure()
plt.hist(df["Act Composite"].dropna(), bins = 20)
plt.title('Histogram of Average ACT Composite Scores in American Universities')
plt.ylabel("Number of Universities")
plt.xlabel("ACT Composite Score")
plt.show()

# Plot histogram of SAT Math and Reading
# Not good
# The distribution of math and reading SAT scores are very similar, with a peak at around 500 points. The values mainly
# extend from around 400 points to around 750 points.
plt.figure()
plt.hist(df['Sat Math'].dropna(), bins=50, alpha=0.5, label='Math')
#plt.hist(df['Sat Writing'].dropna(), bins=50, alpha=0.5, label='Writing')
plt.hist(df['Sat Reading'].dropna(), bins=50, alpha=0.5, label='Reading')
plt.title('Histogram of Average SAT Score in American Universities')
plt.legend()
plt.show()

# Plot histogram of SAT Math and Writing
# Not good
# The distribution of math and writing SAT scores are also very similar, although math scores are slightly higher.
# The most common score for writing is around 450, and math is around 500.
plt.figure()
plt.hist(df['Sat Math'].dropna(), bins=50, alpha=0.5, label='Math')
plt.hist(df['Sat Writing'].dropna(), bins=50, alpha=0.5, label='Writing')
#plt.hist(df['Sat Reading'].dropna(), bins=50, alpha=0.5, label='Reading')
plt.title('Histogram of Average SAT Score in American Universities')
plt.legend()
plt.show()

# Plot Boxplot of SAT
# Each different subject SAT has a very similar mean score, along with similar quartiles. All have a couple outliers.
# Math has the highest mean score, then reading, the writing, but they are all very close.
plt.figure()
plt.boxplot([df['Sat Math'].dropna(),df['Sat Writing'].dropna(), df['Sat Reading'].dropna()])
plt.xticks([1,2,3], ['Sat Math', 'Sat Writing', 'Sat Reading'])
plt.title('Boxplot of Average SAT Score in American Universities')
plt.show()

# Add up the total score of SAT
#
df["Sat Total"] = df["Sat Math"] + df['Sat Writing'] + df['Sat Reading']
print(df['Sat Total'].describe())

# Plot the histogram of SAT total
# The peak is around the middle, with most universities having a total SAT Score of around 1600. However, there is
# quite a large distribution, ranging from 1000 to 2250
plt.figure()
plt.hist(df["Sat Total"].dropna(), bins = 50)
plt.title('Histogram of Total Average SAT Score in American Universities')
plt.xlabel("Number of Universities")
plt.ylabel("Total SAT Score")
plt.show()

# Check which school only accepts high SAT score
print(df[df['Sat Total']>2100]) # Harvard doesn't care so much about Sat Score

# Let's hope to visualize on a map the SAT score distribution across the states of the US

# Convert $500 into 500.0 (dollar to float)
df['In State Tuition'] = df['In State Tuition'].replace('[\$,A-Z]', '', regex=True).astype(float)
df['In State Tuition'].describe()

# Clean out some Schools in Kenya which use KES not USD
print(df[df['In State Tuition']>100000])
df = df.drop(1234)

print(df[df['In State Tuition']>60000])
df = df.drop(25)

# Plot the histogram of in state tutition
# There is a very large range of different average in state tuition. The most common is around 5000, but the highest
# value goes up to around 55000
plt.figure()
plt.hist(df["In State Tuition"].dropna(), bins = 50)
plt.title('Histogram of Average In State Tuition in American Universities')
plt.ylabel("Number of Universities")
plt.xlabel("In State Tuition")
plt.show()

# Histogram of Out of State Tuition
df['Out of State Tuition'] = df['Out of State Tuition'].replace('[\$,A-Z]', '', regex=True).astype(float)
df['Out of State Tuition'].describe()

print(df[df['Out of State Tuition']>100000])
df = df.drop(1553)

# The range of the average out of state tuition for american universities ranges from 0 all the way to around 55000. It
# peaks at around 15000, but is quite high for all values. There are few outliers.
plt.figure()
plt.hist(df["Out of State Tuition"].dropna(), bins = 50)
plt.title('Histogram of Average Out of Tuition in American Universities')
plt.ylabel("Number of Universities")
plt.xlabel("Out of State Tuition")
plt.show()

###########################################################################################################
#Calculate the total students in Public and Private universities
numberofstudents_type = df[["Total Students", "Type"]].copy()
numberofstudents_type = numberofstudents_type.dropna()
numberofstudents_type["Type"].unique()
no_students_private = numberofstudents_type[numberofstudents_type["Type"]== "Private"]
total_student_private = int(no_students_private["Total Students"].sum())
print("Total Students in Private Universities is: " + str(int(no_students_private["Total Students"].sum())))
no_students_public = numberofstudents_type[numberofstudents_type["Type"]== "Public"]
print("Total Students in Public Universities is: " + str(int(no_students_public["Total Students"].sum())))
total_student_public = int(no_students_public["Total Students"].sum())
# Visualize in a pie chart
plt.figure()
plt.axis('equal')
plt.pie([total_student_private, total_student_public], labels = ["Private School", "Public School"],
        autopct=lambda (p): '{:.2f} %'.format(p))
plt.title("Number of Students in Private and Public Schools")
plt.show()

# Compare the acceptance rate between private and public school
ar_type = df[["Acceptance Rate", "Type"]].copy()
ar_type = ar_type.dropna()
ar_type["Type"].unique()
ar_type['Acceptance Rate'] = ar_type['Acceptance Rate'].replace('%', '', regex=True).astype(float)
ar_private = ar_type[ar_type["Type"]== "Private"]
ar_public = ar_type[ar_type["Type"]== "Public"]
plt.figure()
plt.boxplot([ar_private["Acceptance Rate"], ar_public["Acceptance Rate"]])
plt.xticks([1,2], ['Private', 'Public'])
plt.title('Acceptance Rate in Public and Private Universities')
plt.show()


# Compare the tuition between private and public school
tf_type = df[["In State Tuition", "Type"]].copy()
tf_type = tf_type.dropna()
tf_type["Type"].unique()
tf_type['In State Tuition'] = tf_type['In State Tuition'].replace('[\$,A-Z]', '', regex=True).astype(float)
tf_private = tf_type[tf_type["Type"]== "Private"]
tf_public = tf_type[tf_type["Type"]== "Public"]
plt.figure()
plt.boxplot([tf_private["In State Tuition"], tf_public["In State Tuition"]])
plt.xticks([1,2], ['Private', 'Public'])
plt.title('Tuition Fee in Public and Private Universities')
plt.show()


# Homework: Visualize the pie chart for setting.
numberofstudents_setting = df[["Total Students", "Setting"]].copy()
numberofstudents_setting = numberofstudents_setting.dropna()
numberofstudents_setting["Setting"].unique()
no_students_urban = numberofstudents_setting[numberofstudents_setting["Setting"]== "Urban"]
total_student_urban = int(no_students_urban["Total Students"].sum())
print("Total Students in Urban Universities is: " + str(int(no_students_urban["Total Students"].sum())))
no_students_suburban = numberofstudents_setting[numberofstudents_setting["Setting"]== "Suburban"]
total_student_suburban = int(no_students_suburban["Total Students"].sum())
print("Total Students in Suburban Universities is: " + str(int(no_students_suburban["Total Students"].sum())))
no_students_small = numberofstudents_setting[numberofstudents_setting["Setting"]== "Small"]
total_student_small = int(no_students_small["Total Students"].sum())
print("Total Students in Small Universities is: " + str(int(no_students_small["Total Students"].sum())))
no_students_rural = numberofstudents_setting[numberofstudents_setting["Setting"]== "Rural"]
total_student_rural = int(no_students_rural["Total Students"].sum())
print("Total Students in Rural Universities is: " + str(int(no_students_rural["Total Students"].sum())))
plt.figure()
plt.axis('equal')
plt.pie([total_student_urban, total_student_suburban, total_student_rural, total_student_small],
        labels = ["Urban Universities", "Suburban Universities", "Rural Universities", "Small Universities"],
        autopct=lambda (p): '{:.2f} %'.format(p))
plt.title("Number of Students in different types of Universities")
plt.show()


#######################################################################################################
# Plot acceptance rate and SAT Score
ar_sat = df[["Acceptance Rate", "Sat Total"]].copy()
ar_sat = ar_sat.dropna()
# convert the percentage into a number
ar_sat['Acceptance Rate'] = ar_sat['Acceptance Rate'].replace('%', '', regex=True).astype(float)
plt.figure()
plt.scatter(ar_sat["Acceptance Rate"], ar_sat["Sat Total"])
plt.xlabel("Acceptance Rate")
plt.ylabel("Sat Total")
plt.title("Scatter plot of Acceptance Rate compared to Total SAT Score in American Universities")
plt.show()

#Homework: build a linear model and get the formula: Acceptance Rate = b + a*SAT Total
from sklearn.linear_model import LinearRegression
x_column_name = ["Acceptance Rate"]
x = ar_sat[x_column_name]
y = ar_sat["Sat Total"]
lm = LinearRegression()
lm.fit(x, y)
print(lm.intercept_)
print(lm.coef_)
# Acceptance Rate = 153.72 - 0.0562*SAT Total
# SAT Total = 2053.1430 - 6.7727*Acceptance rate

# Plot acceptance rate and in state tuition fee
ar_tf = df[["Acceptance Rate", "In State Tuition"]].copy()
ar_tf = ar_tf.dropna()
ar_tf['Acceptance Rate'] = ar_tf['Acceptance Rate'].replace('%', '', regex=True).astype(float)
plt.figure()
plt.scatter(ar_tf["Acceptance Rate"], ar_tf["In State Tuition"])
plt.xlabel("Acceptance Rate")
plt.ylabel("In State Tuition")
plt.title("Scatter plot of Acceptance Rate compared to In State Tuition in American Universities")
plt.show()

from scipy.stats.stats import pearsonr
pearsonr(ar_tf['In State Tuition'], ar_tf['Acceptance Rate'])

#Homework: build a linear model and get the formula: Acceptance Rate = b + a*In State Tuition
from sklearn.linear_model import LinearRegression
x_column_name = ["Acceptance Rate"]
x = ar_tf[x_column_name]
y = ar_tf["In State Tuition"]
lm = LinearRegression()
lm.fit(x, y)
print(lm.intercept_)
print(lm.coef_)
# Acceptance Rate = 76.490 - 0.00053*In State Tuition
# In State Tuition = 39345.5310 - 287.0930*Acceptance Rate


ts_mr = df[["Total Students", "Male Ratio"]].copy()
ts_mr = ts_mr.dropna()
ts_mr['Male Ratio'] = ts_mr['Male Ratio'].replace('%', '', regex=True).astype(float)
plt.figure()
plt.scatter(ts_mr["Total Students"], ts_mr["Male Ratio"])
plt.xlabel("Total Students")
plt.ylabel("Male Ratio")
plt.title("Scatter plot of Total Students compared to Male Ratio in American Universities")
plt.show()
