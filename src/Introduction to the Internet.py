import socket
mysock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)      # initialise the socket (virtuial pipe of connection
                                                                # between the computer and the internet)
mysock.connect(('www.py4inf.com', 80))                          # tell the socket to connect to a specific server

# sent a GET request to the link
mysock.send('GET http://www.py4inf.com/code/romeo.txt HTTP/1.0\n\n')

# print the data from the link
while True:
    data = mysock.recv(512)
    if (len(data) < 1) :
        break
    print data
mysock.close()

# print the data from the link using urllib library
import urllib
fhand = urllib.urlopen('http://www.py4inf.com/code/romeo.txt')
for line in fhand:
    print line.strip()

# print the words in each line of the data from the link
fhand = urllib.urlopen('http://www.py4inf.com/code/romeo.txt')
counts = dict()
for line in fhand:
    words = line.split()
    for word in words:
        counts[word] = counts.get(word,0) + 1
    print counts

# get the file from the link (failed because there was no such file on the google server)
fhand = urllib.urlopen('www.google.co.uk/intl/en/about.html')
for line in fhand:
    print line.strip()

# download a random html file from the internet
fhand = urllib.urlopen('https://www.cs.tut.fi/~jkorpela/fileurl.html')
for line in fhand:
    print line.strip()

# BeautifulSoup is a library which organises html data for us
import BeautifulSoup as bs
# download html data into html variable
html = urllib.urlopen('https://www.cs.tut.fi/~jkorpela/fileurl.html').read()
# use BeautifulSoup to organise html data
soup = bs.BeautifulSoup(html)
#get all the <a> tags
# (the <a> tag is usually a url from the original site to the other site)
tags = soup('a')
# print all the reference links from the <a> tag
for tag in tags:
    print tag.get('href', None)